{-# LANGUAGE OverloadedStrings #-}

module Runners where

import Scanner
import TokenHelper

import System.Environment
import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import GHC.IO.Handle (hFlush)
import System.IO (stdout)
import qualified Data.Sequence as S
import Parser
import ParseExpressions
import Eval
import AST
import Data.Maybe
import Utils
import ResolverTypes
import Resolver
import EvalTypes
import qualified Data.Vector as V


run :: T.Text -> IO()
run text = do
  scanAndContinue text

runLoxFile :: T.Text -> IO ()
runLoxFile arg = do
  text <- TIO.readFile (T.unpack arg)
  run text

-- TODO: Prompt should remember all variables, and run should use it
-- So a helper function which has a list of argument (all new inputs are added into that)
-- and after the scan, we scan if the tokens has the saved identifier, swap it with their values
-- parse and evaluate
runPrompt :: IO()
runPrompt = do
  input <- readFromRepl
  run (T.pack input)
  runPrompt


startLox :: [T.Text] -> IO ()
startLox args
  | argsLength == 1 = runLoxFile (head args)
  | argsLength > 1 = putStrLn "Use one or less args"
  | otherwise = runPrompt
  where argsLength = length args

startFromTerminal :: IO()
startFromTerminal = do
    args <- getArgs
    let newArgs = map T.pack args
    startLox newArgs


-- Helpers

-- This is the code I used. Thanks Joel Chelliah!
-- https://github.com/joelchelliah/simple-repl-in-haskell/blob/master/README.md
readFromRepl :: IO String
readFromRepl = putStr "Lox> "
     >> hFlush stdout
     >> getLine

scanAndContinue :: T.Text -> IO ()
scanAndContinue text = if null scanError then parseAndContinue tokens else print scanError
  where tokens = scanTokens text
        scanError = S.filter (isNotToken . tokenType) tokens

parseAndContinue :: S.Seq Token -> IO()
parseAndContinue tokens = handleCases 
  where (PROG statements) = parse tokens
        parseError = findParseError statements S.empty
        astError = S.filter isJust (fmap getASTErrorFromStatement statements)
        handleCases
          | (not . null) parseError = print (mconcat ["ParserError: ", show parseError])
          | (not . null) astError = print (mconcat ["ParserError: ", show astError])
          | otherwise = resolveAndContinue (PROG statements)

resolveAndContinue :: AST.PROGRAM -> IO()
resolveAndContinue prog = do
  resolved <- resolveProgram prog
  let errors = rErrors resolved
  if not (S.null errors) then do
    print errors
  else do
    let newProgStack = newDeclarations resolved
    let (newProg, _) = pop newProgStack
    evalAndContinue (PROG newProg) (variableVector resolved)

evalAndContinue :: AST.PROGRAM -> V.Vector EVAL  -> IO ()
evalAndContinue (PROG statements) dMap = evalProgram (PROG statements) dMap
