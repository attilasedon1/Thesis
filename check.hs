askForName :: IO ()
askForName = putStrLn "What is your name"

nameStatement :: String -> String
nameStatement name = "Hello, " ++ name ++ "!"

helloName :: IO()
helloName = askForName >> getLine >>= (\name -> putStrLn (nameStatement name))

main :: IO()
main = helloName
